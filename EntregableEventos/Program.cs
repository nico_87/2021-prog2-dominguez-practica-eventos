﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;

namespace EntregableEventos
{
    class Program
    {
        static void Main(string[] args)
        {
            Principal principal = new Principal();
            principal.EventoAgRl += MensajeAgregadoEliminar;
            principal.EventoDeAgregadoModificado += MensajeConlistado;


        }

        private static void MensajeConlistado(List<string> listado)
        {
            foreach (var item in listado)
            {
                if (item == listado.Last())
                {
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                }
                Console.WriteLine(item);
            }
            Console.Read();
        }

        private static void MensajeAgregadoEliminar(string id, string tipo)
        {
            Console.WriteLine("Producto agregado/Eliminado "+id+tipo);
            
        }

       
    }
}
