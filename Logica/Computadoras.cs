﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Computadoras : Elementos
    {
        public string Descripcion { get; set; }
        public Memoria Ram { get; set; }

        public enum Memoria {dosGB, cuatroGB, ochoGB, dseisGB }

        public bool VerificarRam(int cantMemroria)
        {
            if ((Memoria)cantMemroria== Memoria.dosGB || (Memoria)cantMemroria == Memoria.cuatroGB || (Memoria)cantMemroria == Memoria.ochoGB|| (Memoria)cantMemroria == Memoria.dseisGB)
            {
                return true;
            }
            return false;
        }


    }
}
