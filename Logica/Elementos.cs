﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Elementos
    {
        public string Modelo { get; set; }
        public string Marca { get; set; }
        public int Serie { get; set; }
        public string Id { get; set; }


        public void ObtenerId()
        {
            Id = this.Modelo + "-" + this.Marca + "-" + Convert.ToString(this.Serie);
        }



    }
}
