﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Principal
    {
        public delegate void DlegadoAgregarEliminar(string id, string tipo);
        public event DlegadoAgregarEliminar EventoAgRl;

        public delegate void ProductoAgregadoElimiado(List<string> listado);
        public event ProductoAgregadoElimiado EventoDeAgregadoModificado;



        public EventHandler AccionDispositivo;

        public List<Elementos> Dispositivos { get; set; }

        private static Principal instance = null;

        public Principal()
        {

        }//ver private o public

        public static Principal Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Principal();
                }
                return instance;
            }
        }


        public void AgregarDispositivo(Monitores monitor)
        {
            EventoAgRl(monitor.Id, "Monitor");

        }

        public void AgregarDispositivo(Computadoras Pc)
        {
            
            //Dispositivos.Add();
            EventoAgRl(Pc.Id,"Computadora");

        }

        public bool EliminarDispositivo(string id)
        {
            Elementos dispositivo = Dispositivos.Find(x=>x.Id == id);
            if (dispositivo != null)
            {
                EventoAgRl("","");
                return true;
            }

            return false;
        }

        public string ObtenerDescripcion()
        {
            return "";
        }

        public void ListadoProductoOrdenado()
        {
            List<string>listado ;
            EventoDeAgregadoModificado(listado);
        }

    }
}
